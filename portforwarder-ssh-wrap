#!/bin/bash

# Copyright (c) 2009,2010 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e
set -u


MYLOGNAME="`basename "$0"`[$$]"

usage() {
	echo "local Usage: $0 <source-host> <local-bind> <allowed-port> [<allowed-port> ...]"
	echo "via ssh orig command : forward-to <port>"
}
croak() {
	logger -s -p daemon.warn -t "$MYLOGNAME" "$1"
	exit 1
}


if [ -z "${SSH_ORIGINAL_COMMAND:-}" ] ; then
	echo "Did not find SSH_ORIGINAL_COMMAND" 2>&1
	exit 1
fi


if [ "${1-}" = "-h" ] || [ "${1-}" = "--help" ]; then
	usage
	exit 0
fi
# check/parse local command line
if [ "$#" -le 2 ]; then
	usage >&2
	exit 1
fi
host="$1"
shift
bindaddr="$1"
shift
if [[ "$bindaddr" =~ [^0-9.] ]]; then
	echo "Invalid bindaddr spec" >&2
	exit 1
fi
allowed_ports="$@"

# check/parse remote command line
set "dummy" ${SSH_ORIGINAL_COMMAND}
shift

# check/parse local command line
if [ "$#" != 2 ]; then
	usage >&2
	exit 1
fi
if [ "$1" != "forward-to" ]; then
	croak "Expected forward-to as command"
fi
port="$2"
if [[ "$port" =~ [^0-9] ]]; then
	croak "Invalid port spec"
fi
ok=0
for allowed in $allowed_ports ; do
	if [ "$port" = "$allowed" ]; then
		ok=1
		break
	fi
done

if [ "$ok" = "1" ]; then
	logger -p daemon.info -t "$MYLOGNAME" "Forwarding to port $port for remote host $host"
	exec /bin/nc -q 600 -s "$bindaddr" 127.0.0.1 "$port"
	echo "Exec failed" >&2
	exit 1
else
	croak "$host requested unallowed port $port"
fi
